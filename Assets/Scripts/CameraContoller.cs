﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CameraContoller : MonoBehaviour
{
    public Transform target;

    public Tilemap theMap;
    private Vector3 upperRightLimit;
    private Vector3 lowerLeftLimit;

    private float halfHeight;
    private float halfWidth;

    // Start is called before the first frame update
    void Start()
    {
        //target = PlayerController.instance.transform;
        target = FindObjectOfType<PlayerController>().transform;
        halfHeight = Camera.main.orthographicSize;
        halfWidth = halfHeight * Camera.main.aspect;

        lowerLeftLimit = theMap.localBounds.min + new Vector3(halfWidth, halfHeight, 0f);
        upperRightLimit = theMap.localBounds.max + new Vector3(-halfWidth, -halfHeight, 0f);

        PlayerController.instance.SetBounds(theMap.localBounds.min, theMap.localBounds.max);
    }

    // LateUpdate is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);

        transform.position = new Vector3(Mathf.Clamp(transform.position.x,lowerLeftLimit.x, upperRightLimit.x), Mathf.Clamp(transform.position.y, lowerLeftLimit.y, upperRightLimit.y), transform.position.z);
    }
}
